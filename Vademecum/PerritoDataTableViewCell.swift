//
//  PerritoDataTableViewCell.swift
//  Vademecum
//
//  Created by Emmanuel Noriega Vaca on 01/12/21.
//

import UIKit
import CoreData

class PerritoDataTableViewCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var imgPerrito: UIImageView!
    @IBOutlet weak var lblApodo: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblRaza: UILabel!
    
    @IBOutlet weak var btnAdoptar: UIButton!
    
    var perrito:Animales? = nil
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var delegate: UIViewController? = nil
    
    @IBAction func onAdoptar(_ sender: Any) {
       
        let context = appDelegate.persistentContainer.viewContext
        context.delete(perrito!)
        do{
            try context.save()
            let alert = UIAlertController(title: "Salvando Perritos", message: "Gracias por adoptarlo, te llegara un email con los detalles para recoger.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Listo!!!", style: UIAlertAction.Style.default, handler: nil))
            delegate!.present(alert, animated: true, completion: nil)
            
        }catch let saveError{
            let alert = UIAlertController(title: "Salvando Perritos", message: "No fue posible procesar tu solicitud, vemos que eres muy mala persona", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Sorry!!!", style: UIAlertAction.Style.default, handler: nil))
            delegate!.present(alert, animated: true, completion: nil)
        }
    }
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
