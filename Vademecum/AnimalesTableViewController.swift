//
//  AnimalesTableViewController.swift
//  Vademecum
//
//  Created by Emmanuel Noriega Vaca on 30/11/21.
//

import UIKit
import CoreData
import AssetsLibrary
import Photos

class AnimalesTableViewController: UITableViewController {

   
    
    var plist = UserDefaults.standard
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var animales:[Animales]?
    let pullrefreshcontrol = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animales = fetchPerritos()
        self.refreshControl = pullrefreshcontrol
        
        pullrefreshcontrol.addTarget(self, action: #selector(refreshTable), for: UIControl.Event.valueChanged)
    }
    
    @IBAction func onSalir(_ sender: Any) {
        plist.removeObject(forKey: "usuario")
        let mainStoryBoard = appDelegate.MainStoryBoard()
        
        let viewController = appDelegate.GetViewController(storyboard: mainStoryBoard, ViewControllerName: "Login")
        appDelegate.SetrootViewController(rootViewController: viewController, animate: true, tipo: 1)
    }
   
    @objc func refreshTable(){
        animales = fetchPerritos()
        self.tableView.reloadData()
        pullrefreshcontrol.endRefreshing()
    }


    func fetchPerritos() -> [Animales]? {
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Animales>(entityName: "Animales")
        
        do{
            let animales = try context.fetch(fetchRequest)
            return animales
        }catch let fetchError {
            print("Failed to fetch animales \(fetchError)")
        }
        return nil
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return animales!.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =
            tableView.dequeueReusableCell(withIdentifier:
                                            "cell", for: indexPath) as! AnimalTableViewCell
        
        cell.imgPerrito.image = getImage(urlImage: animales![indexPath.row].imgasset!)
        print(animales![indexPath.row].apodo)
        cell.labelApodo.text = animales![indexPath.row].apodo
        cell.imgPerrito.isOpaque = true
        return cell
    }
   
    
   func  getImage(urlImage:String) -> UIImage? {
       
       // declare your asset url
       let assetUrl = URL(string: urlImage)!
       
       // retrieve the list of matching results for your asset url
       let fetchResult = PHAsset.fetchAssets(withALAssetURLs: [assetUrl], options: nil)

       var imagen:UIImage?
       
       if let photo = fetchResult.firstObject {

           // retrieve the image for the first result
           PHImageManager.default().requestImage(for: photo, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFill, options: nil) {
               image, info in
               /*
               // 3
               let filter = CIFilter(name: "CISepiaTone")
               filter!.setValue(image, forKey: kCIInputImageKey)
               filter!.setValue(0.5, forKey: kCIInputIntensityKey)

               // 4
               let newImage = UIImage(ciImage: filter!.outputImage!)*/
               
               imagen = image
       
           }
       }
       
       return imagen
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DataPerrito" {
            let destination = segue.destination as! PerritoDataTableViewController
            destination.animal = animales?[(tableView.indexPathForSelectedRow?[1])!]
        }
    }
    
    
    

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
