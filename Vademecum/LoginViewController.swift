//
//  LoginViewController.swift
//  Vademecum
//
//  Created by Emmanuel Noriega Vaca on 30/11/21.
//

import UIKit
import CoreData

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var plist = UserDefaults.standard
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    
    @IBAction func onLogin(_ sender: Any) {
        if(txtEmail.text! == "" || txtPass.text! == ""){
            let alerta = UIAlertController(title: "Login", message: "Uno o más campos vacios, favor de validar", preferredStyle: .alert)
            //MARK: Acciones
                    
            alerta.addAction(UIAlertAction(title: "aceptar", style: .default, handler: {
                    (UIAlertAction) in print ("Se le ha dado clic en aceptar")
            }))

            present(alerta, animated: true, completion: nil)
            return
        }
        
        if(!isValidEmail(testStr: txtEmail.text!)){
            //MARK: Crear una alerta
            let alerta = UIAlertController(title: "Login", message: "Email no es válido", preferredStyle: .alert)
            //MARK: Acciones
            alerta.addAction(UIAlertAction(title: "aceptar", style: .default, handler: {
                    (UIAlertAction) in print ("Se le ha dado clic en aceptar")
            }))
            present(alerta, animated: true, completion: nil)
            return
        }
        
        let user = fechUser(email: txtEmail.text!, pass:txtPass.text!)
        
        
        if(user != nil){
            self.plist.set(txtEmail.text!, forKey: "usuario")
            self.appDelegate.OnLoginSuccess()
        }else{
            //MARK: Crear una alerta
            let alerta = UIAlertController(title: "Login", message: "Usuario no encontrado", preferredStyle: .alert)
            //MARK: Acciones
            alerta.addAction(UIAlertAction(title: "aceptar", style: .default, handler: {
                    (UIAlertAction) in print ("Se le ha dado clic en aceptar")
            }))
            present(alerta, animated: true, completion: nil)
            return
        }
    }
    
    
    func fechUser(email:String, pass:String) -> Usuarios? {
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequet = NSFetchRequest<Usuarios>(entityName: "Usuarios")
        fetchRequet.fetchLimit = 1
        fetchRequet.predicate = NSPredicate(format: "email = %@ AND password = %@",argumentArray: [email, pass])
        do{
            let usuario = try context.fetch(fetchRequet)
            return usuario.first
        } catch let createError{
            print ("Failed \(createError)")
            let alert = UIAlertController(title: "Login", message: "Usuario NO ENCONTRADO", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        return nil
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtEmail.delegate = self
        self.txtPass.delegate = self
        txtPass.isSecureTextEntry = true
    }
    
    // MARK: Hide keyboard when user touches outside keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: Press retun key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return (true)
    }

    func isValidEmail(testStr:String) -> Bool {
       let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

       let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
       return emailTest.evaluate(with: testStr)
   }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
