//
//  AppDelegate.swift
//  Vademecum
//
//  Created by Emmanuel Noriega Vaca on 30/11/21.
//

import UIKit
import CoreData

@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    var isAuthenticated = false
    var plist = UserDefaults.standard
    var navController: UINavigationController?
    
    
    func MainStoryBoard() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    func GetViewController(storyboard: UIStoryboard, ViewControllerName:String) -> UIViewController{
        return storyboard.instantiateViewController(withIdentifier: ViewControllerName)
    }
    
    func SetrootViewController(rootViewController:UIViewController, animate: Bool, tipo: Int) {
        if animate {
            if (tipo == 0){
                rootViewController.title = "Adopta un Perrito"
                navController = UINavigationController(rootViewController: rootViewController)
                window?.rootViewController = navController
                window?.makeKeyAndVisible()
            }else{
                window?.rootViewController = rootViewController
            }
            
    
            
            //MARK: Animation
            UIView.transition(with: window!, duration: 0.2, options: .transitionFlipFromRight, animations: {
                if tipo == 0 {
                    rootViewController.title = "Adopta un Perrito"
                    self.navController = UINavigationController(rootViewController: rootViewController)
                    self.window?.rootViewController = self.navController
                    self.window?.makeKeyAndVisible()
                }else{
                    self.window?.rootViewController = rootViewController
                }
            }, completion: nil)
        }else{
               if (tipo == 0){
                   rootViewController.title = "Adopta un Perrito"
                   navController = UINavigationController(rootViewController: rootViewController)
                   window?.rootViewController = navController
                   window?.makeKeyAndVisible()
               }else{
                   window?.rootViewController = rootViewController
               }
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      
        let context = self.persistentContainer.viewContext
        
        //MARK: Si ya esta autenticado
        let usuario = plist.string(forKey: "usuario")
        
        if let usu = usuario {
            
         
            let fetchRequet = NSFetchRequest<Usuarios>(entityName: "Usuarios")
            fetchRequet.fetchLimit = 1
            fetchRequet.predicate = NSPredicate(format: "email = %@", usu)
            do{
                if (try context.fetch(fetchRequet)) != nil {
                    isAuthenticated = true
                }
              
            } catch let createError{}
        }
        
        if isAuthenticated {
            let viewController = GetViewController(storyboard: MainStoryBoard(), ViewControllerName: "Principal")
            SetrootViewController(rootViewController: viewController, animate:  false, tipo: 0)
        }else{
            let loginViewController = GetViewController(storyboard: MainStoryBoard(), ViewControllerName: "Login") as! LoginViewController
            OnLoginSuccess()
            SetrootViewController(rootViewController: loginViewController, animate: true, tipo:1)
        }
        return true
        
    }
    func OnLoginSuccess(){
        let viewController = GetViewController(storyboard: MainStoryBoard(), ViewControllerName: "Principal")
        SetrootViewController(rootViewController: viewController, animate:  false, tipo: 0)
    }
    
    
  
    
 

    
    

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Vademecum")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

