//
//  RegisterViewController.swift
//  Vademecum
//
//  Created by Emmanuel Noriega Vaca on 30/11/21.
//

import UIKit
import CoreData

class RegisterViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtPassword2: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtPassword2.delegate = self
        
        txtPassword.isSecureTextEntry = true
        txtPassword2.isSecureTextEntry = true

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func onRegister(_ sender: Any) {
    
        
        if(txtEmail.text! == "" || txtPassword.text! == "" || txtPassword2.text! == ""){
            //MARK: Crear una alerta
            let alerta = UIAlertController(title: "Registro", message: "Existen campos vacios, validar", preferredStyle: .alert)
            //MARK: Acciones
                    
            alerta.addAction(UIAlertAction(title: "aceptar", style: .default, handler: {
                    (UIAlertAction) in print ("Se le ha dado clic en aceptar")
            }))
            present(alerta, animated: true, completion: nil)
            return
            
        }
        
        
        if(!isValidEmail(testStr: txtEmail.text!)){
            //MARK: Crear una alerta
            let alerta = UIAlertController(title: "Registro", message: "Email no es válido", preferredStyle: .alert)
            //MARK: Acciones
                    
            alerta.addAction(UIAlertAction(title: "aceptar", style: .default, handler: {
                    (UIAlertAction) in print ("Se le ha dado clic en aceptar")
            }))

            present(alerta, animated: true, completion: nil)
            
            return
            
        }
        
        
        if(txtPassword.text! != txtPassword2.text!){
            //MARK: Crear una alerta
            let alerta = UIAlertController(title: "Registro", message: "Las contraseñas proporcionadas no coinciden", preferredStyle: .alert)
            //MARK: Acciones
            alerta.addAction(UIAlertAction(title: "aceptar", style: .default, handler: {
                    (UIAlertAction) in print ("Se le ha dado clic en aceptar")}))
            present(alerta, animated: true, completion: nil)
            return
        }
        
       
            let context = appDelegate.persistentContainer.viewContext
            
            let newUser = NSEntityDescription.insertNewObject(forEntityName: "Usuarios", into: context) as! Usuarios
            
            newUser.email = txtEmail.text
            newUser.password = txtPassword.text
            newUser.username = txtEmail.text
            do{
                try context.save()
                let alert = UIAlertController(title: "Nuevo Usuario", message: "Usuario Registrado correctamente.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                txtEmail.text = ""
                txtPassword.text = ""
                txtPassword2.text = ""
                
            }catch let createError{
                print ("Failed \(createError)")
                let alert = UIAlertController(title: "Nuevo Usuario", message: "Usuario NO registrado correctamente.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
    }
    
    func isValidEmail(testStr:String) -> Bool {
       let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

       let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
       return emailTest.evaluate(with: testStr)
   }
    
    
    // MARK: Hide keyboard when user touches outside keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: Press retun key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return (true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
