//
//  AnimalTableViewCell.swift
//  Vademecum
//
//  Created by Emmanuel Noriega Vaca on 30/11/21.
//

import UIKit
import CoreData
    
class AnimalTableViewCell: UITableViewCell {

    @IBOutlet weak var imgPerrito: UIImageView!
    @IBOutlet weak var labelApodo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
    }
}
