//
//  AltaPerroViewController.swift
//  Vademecum
//
//  Created by Emmanuel Noriega Vaca on 30/11/21.
//

import UIKit
import CoreData
import AssetsLibrary
import Photos

class AltaPerroViewController: UIViewController,  UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var imgUrl:String = ""
    @IBOutlet weak var txtApodo: UITextField!
    @IBOutlet weak var txtEdad: DesignableView!
    @IBOutlet weak var txtEdadCanina: UITextField!
    @IBOutlet weak var txtContacto: UITextField!
    @IBOutlet weak var txtDescripcion: UITextField!
    
    @IBOutlet weak var pickerRazas: UIPickerView!
    
    @IBOutlet weak var imgPerrito: UIImageView!
    
    
    let pickerData = ["Pequeña", "Mediana", "Grande", "Gigante"]
    
    @IBAction func onSelectImage(_ sender: Any) {
        let imagePikcer = UIImagePickerController()
        present(imagePikcer, animated: true, completion: nil)
        imagePikcer.delegate = self
        
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Connect data:
        self.pickerRazas.delegate = self
        self.pickerRazas.dataSource = self
        // Do any additional setup after loading the view.
        self.txtApodo.delegate = self
        self.txtEdadCanina.delegate = self
        self.txtContacto.delegate = self
        self.txtDescripcion.delegate = self
        
    }
    
    @IBAction func onRegistrar(_ sender: Any) {
        
  
        
        if(txtApodo.text! == "" || txtEdadCanina.text! == "" || txtContacto.text! == "" || txtDescripcion.text! == "" || imgUrl == ""){
            //MARK: Crear una alerta
            let alerta = UIAlertController(title: "Registro Perrito", message: "Existen campos vacios, validar", preferredStyle: .alert)
            //MARK: Acciones
                    
            alerta.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: {
                    (UIAlertAction) in print ("Se le ha dado clic en aceptar")
            }))
            present(alerta, animated: true, completion: nil)
            return
            
        }
        
        if(!validate(txtEdadCanina.text!, pattern: "^-?[0-9]+")){
            let alerta = UIAlertController(title: "Registro Perrito", message: "La edad ingresada no es valida", preferredStyle: .alert)
            //MARK: Acciones
                    
            alerta.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: {
                    (UIAlertAction) in print ("Se le ha dado clic en aceptar")
            }))
            present(alerta, animated: true, completion: nil)
            return
        }
        
        saveImage()
        
        let context = appDelegate.persistentContainer.viewContext
        
        let newPerrito = NSEntityDescription.insertNewObject(forEntityName: "Animales", into: context) as! Animales
        
        newPerrito.apodo = txtApodo.text
        newPerrito.desc = txtDescripcion.text
        newPerrito.telefono = txtContacto.text
        newPerrito.raza = pickerData[pickerRazas.selectedRow(inComponent: 0)] as String
        newPerrito.edad = txtEdadCanina.text
        newPerrito.imgasset = imgUrl
        
    
        do{
            try context.save()
            let alert = UIAlertController(title: "Salvando Perritos", message: "Nuevo perrito registrado  correctamente.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            txtApodo.text = ""
            txtDescripcion.text = ""
            txtContacto.text = ""
            txtEdadCanina.text = ""

           
            
        }catch let createError{
            print ("Failed \(createError)")
            let alert = UIAlertController(title: "Salvando Perritos", message: "No fue posible registrar, eres mala persona.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func validate(_ str: String, pattern: String) -> Bool {
        if let range = str.range(of: pattern, options: .regularExpression) {
            let result = str.substring(with: range)
            print(result)
            return true
        }
        return false
    }

    
    
    
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
          return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return pickerData.count
    }
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
           return pickerData[row]
    }
    

    // MARK: Hide keyboard when user touches outside keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: Press retun key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return (true)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let imageURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
                let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
                let asset = result.firstObject
                // MARK: Guardamos la URL de la imagen
                imgUrl = imageURL.absoluteString
                print(imgUrl)
            }

        imgPerrito.image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage?
            dismiss(animated: true, completion: nil)
        
    }
    
    
    func saveImage(){
        guard let image = imgPerrito.image else { return }
        UIImageWriteToSavedPhotosAlbum(image, self,#selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
       
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        
        if let error = error {
               let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
               ac.addAction(UIAlertAction(title: "OK", style: .default))
               present(ac, animated: true)
           } else {
               let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
               ac.addAction(UIAlertAction(title: "OK", style: .default))
               present(ac, animated: true)
           }
    }
}
