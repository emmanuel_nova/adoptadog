//
//  CustomTextField.swift
//  Vademecum
//
//  Created by Emmanuel Noriega Vaca on 30/11/21.
//

import UIKit

class CustomTextFiled: UITextField {
    
    private let underline = CALayer()
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        addPadding()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        addPadding()
    }
  
    private func setupUnderline() {
        // 1
        borderStyle = .none
            
        // 2
        let lineWidth: CGFloat = 1.0
        underline.borderColor = UIColor.darkGray.cgColor
        underline.frame = CGRect(
            x: 0,
            y: frame.size.height - lineWidth,
            width: frame.size.width,
            height: frame.size.height
        )
        underline.borderWidth = lineWidth
            
        // 3
        layer.addSublayer(underline)
        layer.masksToBounds = true
    }

    override func setNeedsLayout() {
        super.setNeedsLayout()
        setupUnderline()
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: 35.0)
    }
    
    private func addPadding() {
        // 1
        rightViewMode = .always
        leftViewMode = .always

        // 2
        let paddingView = UIView(
            frame: CGRect(
                x: 0,
                y: 0,
                width: 12,
                height: 35
            )
        )

        // 3
        leftView = paddingView
        rightView = paddingView
    }

}
